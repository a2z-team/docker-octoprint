# docker-octoprint
OctoPrint Docker container, Thanks to : http://octoprint.org/

```
docker run -d -p 5000 --device=/dev/ttyUSB0 --name=OctoPrint atwoz/OctoPrint
```

NOTE: To add camera support, please add forward the device to the container using --device argument

---
CuraEngine Integration
--
CuraEngine is installed under:
```
/CuraEngine/CuraEngine
```
Please set it in the settings menu if you intend to use it.

---
Webcam Integration
---
1. First, bind the camera to the docker using --device=/dev/video0:/dev/videoX where videoX is your camera device on linux.
2. If camera supports only MJPEG formatting, please set YUV_CAMERA to false.
3. should add to config.yaml/Or UI:
```
webcam:
  stream: http://127.0.0.1:8088/?action=stream
  snapshot: http://127.0.0.1:8088/?action=snapshot
  ffmpeg: /usr/bin/avconv
```
NOTE: This is just basic support, need to add HAProxy to make it properly.
HAProxy Instructions: https://github.com/foosel/OctoPrint/wiki/Setup-on-a-Raspberry-Pi-running-Raspbian#make-everything-accessible-on-port-80
